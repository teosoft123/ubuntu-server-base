# Hypervisor based on Ubuntu 16.04.2 Server

* Create Linux VM named called ubuntu-server-base-1604-lts in VirtualBox with
    _80GB QEMU drive, 30GB system partition, 1GB RAM, KVM virtualization interface and host port 2022 forwarded to guest port 22_
 
* Install Ubuntu 16.04.2 Server with 
    _standard system utilities_, _OpenSSH Server_ and _Virtual Machine host_ options.
    
* Enable root login with SSH with key only (it's default in 16)
* Add SSH keys for user root
* Update system (apt update ; apt upgrade)
* Install ntp and ntpdate
* Make a full clone of everything, named it ubuntu-server-base-1604-stage-1
* Remove user installer
* Install VirtualBox Guest additions (as root):

    apt-get install -y linux-headers-$(uname -r) build-essential dkms -y
	wget http://download.virtualbox.org/virtualbox/5.1.14/VBoxGuestAdditions_5.1.14.iso
	mkdir /media/VBoxGuestAdditions
	mount -o loop,ro VBoxGuestAdditions_5.1.14.iso /media/VBoxGuestAdditions
	sh /media/VBoxGuestAdditions/VBoxLinuxAdditions.run
	rm VBoxGuestAdditions_5.1.14.iso
	umount /media/VBoxGuestAdditions
	rmdir /media/VBoxGuestAdditions
	apt-get remove -y linux-headers-$(uname -r) build-essential dkms

* Run apt autoremove; apt clean
* Clean root bash history like this: 
    cat /dev/null > ~/.bash_history && history -c && shutdown -h 0
* zerofree if image is too big 

## Done with image, can create Vagrant box!

    vagrant package --base ubuntu-server-lts --output ubuntu-server-base-1604-lts.box

## Now, add the box:

	vagrant box add --provider virtualbox --name teosoft/ubuntu-server-base-1604-lts ubuntu-server-base-1604-lts.box
 
## Create Vagrantfile:

    vagrant init teosoft/ubuntu-server-base-1604-lts
    
## Now you need to add private key for the vagrant to connect to it, at the top level

    config.ssh.username = "root"
    config.ssh.private_key_path = File.join(File.dirname(__FILE__),"keys/ubuntu-base-ssh-backup-key")

## Now you can run ansible or shell provisioner to configure the new VM to your liking!

## Do not forget:

* To change server name
* To regenerate server keys

TODO create a script that regenerates SSH server keys on a first boot. Will take care of one of the above.


## That's all folks!
